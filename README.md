# Hermes

## Exchange messages between different platforms in a pluggable manner

There are basically three concepts around the Hermes project:

- **Origins**: modules to abstract the platform that I will get the messages from
- **Destinations**: modules to abstract the platform that I will deliver the messages to
- **Sandals**: programs that will actually carry the message from one side to the other

The analogy comes from the Greek god Hermes and the fact that he is known as the
messenger of the gods, and normally is depicted using flying sandals.

As in Mount Olympus, messages delivered to the gods can be _tampered_. In
the data pipelines' world we call it _transformation_. Hermes allows transformations
between origin and destination using Python UDFs.

## Currently supported platforms

### Origins

**Apache Kafka (avro, with Schema Registry)**

Built on top of the official python SDK ([confluent-kafka-python](https://github.com/confluentinc/confluent-kafka-python)) for Apache Kafka.

### Destinations

**AWS: Kinesis Data Streams, S3**

Built on top of the official python SDK ([boto3](https://github.com/boto/boto3)) for Amazon Web Services.

**PostgreSQL databases**

Built on top of [psycopg2](https://www.psycopg.org/) python library, using a hybrid approach (copy + insert).

## Tests

Make sure you have Docker and Docker Compose installed and configured. Then run

```shell
docker compose up -d
```

It will spin up the test infrastructure:

- [fast-data-dev](https://github.com/lensesio/fast-data-dev): developing environment
for Apache Kafka

- [localstack](https://github.com/localstack/localstack): Amazon Web Services emulated
environment

- [postgres](https://hub.docker.com/_/postgres): A PostgreSQL development database

Run your tests from the root directory of the project:

```shell
poetry run python -m pytest tests
```

## Roadmap

- More connectors (probably S3 will be the next)
- More tests
