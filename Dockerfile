FROM ubuntu:20.04
ENV PYTHONPATH "/hermes:${PYTHONPATH}"
COPY . .
RUN apt-get update \
    && apt-get install -y python3-pip libpq-dev \
    && pip3 install --no-cache -r requirements.txt
ENTRYPOINT [ "python3", "hermes/sandals.py" ]
