from collections import namedtuple
from hermes.config import consumer_conf, sr_conf, KAFKA_TOPIC
from collections import namedtuple
from confluent_kafka import TopicPartition
from origins.kafka.avro_consumer import AvroConsumer
from origins.kafka.offset.offset_persister import OffsetPersister
from origins.generic_loop.generic_loop_consumer import GenericLoopConsumer
from destinations.courier import Courier
import logging
import os
import tamper

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO')
logging.basicConfig(level=logging.__dict__[LOGLEVEL])


def _on_revoke(consumer, partitions):
    current_offsets = consumer.position(partitions)
    Offset = namedtuple('Offset', 'offset persisted')
    offsets = {tp.partition: Offset(tp.offset, False) for tp in current_offsets if tp.offset > 0}
    logging.debug(f'Current offsets: {offsets}')
    op = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    op.persist(offsets)
    logging.warning(f'Partitions revoked: {[tp.partition for tp in current_offsets]}')

def _on_assign(consumer, partitions):
    topic = partitions[0].topic
    partition_numbers = [p.partition for p in partitions]
    op = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    loff = op.get_last_offsets(partition_numbers)
    consumer.assign(
        [TopicPartition(topic, part, offs) for part, offs in loff.items()]
    )
    logging.info(f'Partitions assigned: {partition_numbers}')
    logging.warning(f'Current partitions: {[a.partition for a in consumer.assignment()]}')

def sandals_on_kinesis():

    offp = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    cour = Courier('kinesis', region='us-east-1')

    ac = AvroConsumer(
        sr_conf=sr_conf,
        consumer_conf=consumer_conf,
        topic=KAFKA_TOPIC,
        courier=cour,
        offset_persister=offp,
        value_schema_subject=KAFKA_TOPIC+'-value'
    )

    ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    ac.consume()

def sandals_on_postgres():

    offp = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    cour = Courier(
        'postgres',
        table='test',
        fields=['id', 'value', 'value_plus_1', 'metadata'],
        tamper_function=tamper.add1
    )
    ac = AvroConsumer(
        sr_conf=sr_conf,
        consumer_conf=consumer_conf,
        topic=KAFKA_TOPIC,
        courier=cour,
        offset_persister=offp,
        value_schema_subject=KAFKA_TOPIC+'-value'
    )
    ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    ac.consume()

def sandals_on_s3():

    offp = OffsetPersister('dynamodb', KAFKA_TOPIC, region='us-east-1')
    cour = Courier(
        's3',
        region='us-east-1',
        tamper_function=tamper.add1
    )
    ac = AvroConsumer(
        sr_conf=sr_conf,
        consumer_conf=consumer_conf,
        topic=KAFKA_TOPIC,
        courier=cour,
        offset_persister=offp,
        value_schema_subject=KAFKA_TOPIC+'-value'
    )
    ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
    ac.consume_until(minutes=2, memory=100)


def sandals_on_generic_loop():

    from uuid import uuid4

    def foo_func():
        messages = []
        for i in range(1000):
            messages.append(
                {
                    'id': str(uuid4()),
                    'value': i
                }
            )
        return messages

    cour = Courier(
        'void'
    )

    glc = GenericLoopConsumer(
        courier=cour,
        consumer_function=foo_func,
        wait_time_seconds=60
    )

    glc.consume()

if __name__ == '__main__':
    sandals_on_generic_loop()
