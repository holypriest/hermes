consumer_conf = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': 'hermes-group',
    'enable.auto.commit': 'false'
}

sr_conf = {
    'url': 'http://localhost:8081'
}

KAFKA_TOPIC = 'test-topic'
MAX_BUFFER_SIZE = 1000
KINESIS_STREAM = 'hermes-kafka-kinesis-stream'
KAFKA_DYNAMODB_OFFSETS_TABLE = 'hermes-kafka-dynamodb-offsets'

POSTGRES_HOST = 'localhost'
POSTGRES_DATABASE = 'postgres'
POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = 'secret'

S3_BUCKET = 'hermes-kafka-s3-bucket'
S3_PATH = 's3-courier'
