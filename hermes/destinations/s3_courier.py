import boto3
from hermes.config import S3_BUCKET, S3_PATH
from datetime import datetime
import gc
import io
import json
import time
import uuid

def iterable_to_stream(iterable, buffer_size=io.DEFAULT_BUFFER_SIZE):
    """
    Lets you use an iterable (e.g. a generator) that yields bytestrings as a read-only
    input stream.
    """
    class IterStream(io.RawIOBase):
        def __init__(self):
            self.leftover = None
        def readable(self):
            return True
        def readinto(self, b):
            try:
                l = len(b)  # We're supposed to return at most this much
                chunk = self.leftover or next(iterable)
                output, self.leftover = chunk[:l], chunk[l:]
                b[:len(output)] = output
                return len(output)
            except StopIteration:
                return 0    # indicate EOF
    return io.BufferedReader(IterStream(), buffer_size=buffer_size)

class S3Courier:
    """Courier that persists data to an AWS S3 bucket"""

    def __init__(self, bucket=None, path=None, region='us-east-1', tamper_function=None):
        self.region = region
        self.resource = self._get_resource(region)
        self.bucket = bucket or S3_BUCKET
        self.path = path or S3_PATH
        self.tamper_function = tamper_function
        self.messages = []

    def _get_resource(self, region):
        s3 = boto3.resource('s3', region_name=region)
        return s3

    def _get_filename(self):
        object_path = '/'.join(
            [
                self.path,
                str(datetime.now().date()),
                str(int(time.time())) + '-' + str(uuid.uuid4()) + '.json'
            ]
        )
        return object_path

    def collect_messages(self, buffer):
        if self.tamper_function:
            self.messages = [self.tamper_function(msg) for msg in buffer]
        else:
            self.messages = buffer

    def deliver(self):
        obj = self.resource.Object(self.bucket, self._get_filename())
        obj.put(
            Body=iterable_to_stream(
                ((json.dumps(msg) + '\n').encode('utf-8')
                for msg in self.messages)
            ).read()
        )
        del self.messages
        gc.collect()
        self.messages = []
        return {'status': 'delivered'}
