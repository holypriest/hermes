from hermes.config import POSTGRES_HOST, POSTGRES_DATABASE, POSTGRES_USER, POSTGRES_PASSWORD
from datetime import datetime
import io
import json
import logging
import psycopg2
import psycopg2.extras
import psycopg2.errors
from typing import Iterator, Optional


class StringIteratorIO(io.TextIOBase):
    """Iterator that delivers one StringIO row at a time"""
    def __init__(self, iter: Iterator[str]):
        self._iter = iter
        self._buff = ''

    def readable(self) -> bool:
        return True

    def _read1(self, n: Optional[int] = None) -> str:
        while not self._buff:
            try:
                self._buff = next(self._iter)
            except StopIteration:
                break
        ret = self._buff[:n]
        self._buff = self._buff[len(ret):]
        return ret

    def read(self, n: Optional[int] = None) -> str:
        line = []
        if n is None or n < 0:
            while True:
                m = self._read1()
                if not m:
                    break
                line.append(m)
        else:
            while n > 0:
                m = self._read1(n)
                if not m:
                    break
                n -= len(m)
                line.append(m)
        return ''.join(line)


class PostgresCourier:
    """Courier that persists data to a PostgreSQL database"""

    def __init__(self, table, fields, tamper_function=None, host=None, database=None, user=None, password=None):
        self.table = table
        self.fields = fields
        self.tamper_function = tamper_function
        self.host = host if host else POSTGRES_HOST
        self.database = database if database else POSTGRES_DATABASE
        self.user = user if user else POSTGRES_USER
        self.password = password if password else POSTGRES_PASSWORD
        self.connection = self._get_connection()
        self.messages = []

    def _get_connection(self):
        connection = psycopg2.connect(
            host=self.host,
            database=self.database,
            user=self.user,
            password=self.password,
        )
        return connection

    def collect_messages(self, buffer):
        if self.tamper_function:
            self.messages = [self.tamper_function(msg) for msg in buffer]
        else:
            self.messages = buffer

    def _prepare_csv(self, field):
        if field is None:
            return '\\N'
        return str(field)

    def _copy_string_iterator(self):
        """
        Save messages into PG table using CSV copy from approach
        If a duplicate key is found, fallback to insert using batch approach"""
        try:
            with self.connection.cursor() as cursor:
                messages_string_iterator = StringIteratorIO(
                    (
                        '|'.join(
                            [self._prepare_csv(msg[field]) for field in self.fields]
                        ) + '\n'
                        for msg in self.messages
                    )
                )
                cursor.copy_from(messages_string_iterator, self.table, sep='|')
                self.connection.commit()
        except psycopg2.errors.UniqueViolation as e:
            logging.info(e)
            logging.info('Primary key violation, falling back to insert method')
            self.connection.rollback()
            field_names = ', '.join(self.fields)
            field_placeholders = ', '.join([f'%({field})s' for field in self.fields])
            with self.connection.cursor() as cursor:
                psycopg2.extras.execute_batch(cursor, f"""
                    INSERT INTO {self.table} ({field_names})
                    VALUES ({field_placeholders})
                    ON CONFLICT DO NOTHING;
                """, self.messages, page_size=1000)
                self.connection.commit()

    def deliver(self):
        self._copy_string_iterator()
        self.messages = []
        return {'status': 'delivered'}
