from .kinesis_courier import KinesisCourier
from .postgres_courier import PostgresCourier
from .s3_courier import S3Courier
from .void_courier import VoidCourier


class Courier:
    """Represents a generic courier, implementing the courier interface. This
    class should be capable of instantiating every available courier

    Attributes
    ----------
    courier : any available courier object

    Methods
    -------
    collect_messages: get a pointer to the message buffer
    deliver: basic interface to persist data

    """

    def __init__(self, courier_type, **kwargs):
        self.courier = self._get_courier(courier_type, **kwargs)

    def _get_courier(self, courier_type, **kwargs):
        if courier_type == 'kinesis':
            return KinesisCourier(
                stream=kwargs.get('stream'),
                region=kwargs.get('region', 'us-east-1'),
                tamper_function = kwargs.get('tamper_function')
            )
        elif courier_type == 'postgres':
            return PostgresCourier(
                table = kwargs.get('table'),
                fields = kwargs.get('fields'),
                tamper_function = kwargs.get('tamper_function')
            )
        elif courier_type == 's3':
            return S3Courier(
                region=kwargs.get('region', 'us-east-1'),
                bucket = kwargs.get('bucket'),
                path = kwargs.get('path'),
                tamper_function = kwargs.get('tamper_function')
            )
        elif courier_type == 'void':
            return VoidCourier()

    def collect_messages(self, buffer):
        self.courier.collect_messages(buffer)

    def deliver(self):
        self.courier.deliver()
