import logging


class VoidCourier:
    """Courier that logs messages to stdout"""

    def __init__(self):
        self.messages = []
        self.tamper_function = None

    def collect_messages(self, buffer):
        if self.tamper_function:
            self.messages = [self.tamper_function(msg) for msg in buffer]
        else:
            self.messages = buffer

    def deliver(self):
        print('Delivering messages:')
        for message in self.messages:
            logging.info(message)
