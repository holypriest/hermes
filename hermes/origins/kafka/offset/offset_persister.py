from hermes.origins.kafka.offset.dynamo_db_offset_persister \
    import DynamoDBOffsetPersister
from hermes.origins.kafka.offset.void_persister import VoidPersister


class OffsetPersister:
    """Represents a generic Apache Kafka offset persister, implementing
    the persister interface. This class should be capable of instantiating
    every available persister

    Attributes
    ----------
    topic : Apache Kafka topic name
    persister: any available persister object

    Methods
    -------
    persist(offsets=dict): basic interface to persist Apache Kafka offsets

    get_last_offsets(partition_numbers=list(int)): list of partition numbers
    to get the last offsets from
    """

    def __init__(self, persister_type, topic, **kwargs):
        self.topic = topic
        self.persister = self._get_persister(persister_type, **kwargs)

    def _get_persister(self, persister_type, **kwargs):
        if persister_type == 'dynamodb':
            return DynamoDBOffsetPersister(
                self.topic,
                tbl_name=kwargs.get('tbl_name'),
                region=kwargs.get('region')
            )
        elif persister_type == 'void':
            return VoidPersister(self.topic)

    def persist(self, offsets):
        """
        Persist Apache Kafka offsets

        Parameters
            offsets: dict ::: {'partition_number': 'offset'}
        """

        self.persister.persist(offsets)

    def get_last_offsets(self, partition_numbers):
        """
        Get the last persisted Apache Kafka offsets per partition

        Paramaters:
            partition_numbers: list(int)
        """

        return self.persister.get_last_offsets(partition_numbers)
