import logging


class VoidPersister:
    """Offset persister that logs offset to stdout"""

    def __init__(self, topic):
        self.topic = topic

    def persist(self, offsets):
        for partition, offset in offsets.items():
            if not offset.persisted:
                logging.info(f'Persisting offset: {(partition, offset)}')

    def get_last_offsets(self, partition_numbers):
        pass
