import boto3
from boto3.dynamodb.conditions import Key, Attr
from hermes.config import KAFKA_DYNAMODB_OFFSETS_TABLE
import logging


class DynamoDBOffsetPersister:
    """Persists Apache Kafka offsets to an AWS DynamoDB table"""

    def __init__(self, topic, tbl_name=None, region='us-east-1'):
        self.topic = topic
        table_name = tbl_name if tbl_name \
            else KAFKA_DYNAMODB_OFFSETS_TABLE
        self.table = self._get_table(table_name, region)

    def _get_table(self, table_name, region):
        dynamodb = boto3.resource('dynamodb', region_name=region)
        return dynamodb.Table(table_name)

    def persist(self, offsets):
        for partition, offset in offsets.items():
            if not offset.persisted:
                response = self.table.update_item(
                    Key={
                        'topic': self.topic,
                        'partition': partition,
                    },
                    UpdateExpression="set partition_offset = :off",
                    ExpressionAttributeValues={':off': offset.offset},
                    ReturnValues="NONE"
                )
                logging.debug(f'DynamoDB persist response: {response}')

        persisted_offsets = [
            (k, v.offset) for k, v in offsets.items()
            if v.persisted is False
        ]

        if persisted_offsets:
            logging.warning(f'Persisted offsets: {persisted_offsets}')

    def get_last_offsets(self, partition_numbers):

        response = self.table.scan(
            FilterExpression=Key('topic').eq(self.topic) &
            Attr('partition').is_in(partition_numbers)
        )
        logging.info(f'DynamoDB get offsets response: {response}')

        last_offsets = {
            int(i['partition']): int(i['partition_offset'])
            for i in response['Items']
        }

        for pn in partition_numbers:
            if pn not in last_offsets.keys():
                last_offsets[pn] = 0

        return last_offsets
