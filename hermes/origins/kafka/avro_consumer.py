from hermes.config import MAX_BUFFER_SIZE
from collections import namedtuple
from confluent_kafka.avro.serializer import SerializerError
from confluent_kafka import TopicPartition
from confluent_kafka import DeserializingConsumer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
import gc
import json
import logging
import time


Offset = namedtuple('Offset', 'offset persisted')


class AvroConsumer:
    """Builds an AvroConsumer on the top of a DeserializingConsumer

    Attributes
    ----------
    schema_registry_client : self-explanatory
    consumer: a DeserializingConsumer object instance
    topic: an Apache Kafka topic name
    courier: a generic Courier instance
    offset_persister: a generic OffsetPersister instance
    value_schema_subject: Schema Registry value subject name
    key_schema_subject: Schema Registry key subject name

    """

    def __init__(
        self,
        sr_conf,
        consumer_conf,
        topic,
        courier,
        offset_persister,
        value_schema_subject,
        key_schema_subject=None,
        commit_offsets_to_kafka=False
    ):
        self.schema_registry_client = SchemaRegistryClient(sr_conf)
        self.consumer = self._get_consumer(
            consumer_conf,
            value_schema_subject,
            key_schema_subject
        )
        self.topic = topic
        self.courier = courier
        self.offset_persister = offset_persister
        self.max_offsets = {}
        self.commit_offsets_to_kafka = commit_offsets_to_kafka

    def _get_consumer(
        self,
        consumer_conf,
        value_schema_subject,
        key_schema_subject
    ):
        if key_schema_subject:
            key_schema_str = self.schema_registry_client \
                .get_latest_version(key_schema_subject) \
                .schema.schema_str
            avro_key_deserializer = AvroDeserializer(
                key_schema_str,
                self.schema_registry_client
            )
            consumer_conf['key.deserializer'] = avro_key_deserializer

        value_schema_str = self.schema_registry_client \
            .get_latest_version(value_schema_subject) \
            .schema.schema_str
        avro_value_deserializer = AvroDeserializer(
            value_schema_str,
            self.schema_registry_client
        )

        consumer_conf['value.deserializer'] = avro_value_deserializer

        return DeserializingConsumer(consumer_conf)

    def _get_time_diff(self, init, end):
        """Returns the time between two consecutive writes, in minutes"""
        return (end - init) / 60

    def _get_message_size_in_mb(self, msg_value):
        """Returns the approximated buffer size, in MB (assuming JSON format)"""
        return len(json.dumps(msg_value)) / (1024 ** 2)

    def subscribe(self, on_assign=None, on_revoke=None):
        """Subscribe the consumer instance to an Apache Kafka topic"""
        self.consumer.subscribe(
            [self.topic],
            on_assign=on_assign,
            on_revoke=on_revoke
        )

    def commit_offsets(self):
        """Commit last consumed message offsets to Apache Kafka topic"""
        offsets_to_commit = []
        for partition, offset in self.max_offsets.items():
            if not offset.persisted:
                offsets_to_commit.append(
                    TopicPartition(self.topic, partition, offset.offset)
                )
                self.max_offsets[partition] = Offset(
                    offset=offset.offset, persisted=True
                )
        if self.commit_offsets_to_kafka:
            self.consumer.commit(offsets=offsets_to_commit)

    def mark_revoked_as_persisted(self):
        """Set offset status as persisted for revoked partitions"""
        topic = self.consumer.list_topics(self.topic).topics[self.topic]
        all_partitions = set([p for p in topic.partitions.keys()])
        current_partitions = set([tp.partition for tp in self.consumer.assignment()])
        old_partitions = all_partitions.difference(current_partitions)
        for op in old_partitions:
            if self.max_offsets.get(op):
                self.max_offsets[op] = Offset(self.max_offsets[op].offset, True)

    def consume(self):
        """Executes the consumer infinite loop (delivery based on buffer length)"""

        buffer = []

        while True:

            try:
                msg = self.consumer.poll(2)
            except SerializerError as e:
                logging.error(f'Deserialization failed for {msg}: {e}')
                break

            if msg is None:
                logging.info(f'No message in this poll ::: Buffer length: {len(buffer)}')
                if buffer:
                    logging.warning(f'Ingesting partially ::: Buffer length: {len(buffer)}')
                    self.courier.collect_messages(buffer)
                    self.courier.deliver()
                    self.mark_revoked_as_persisted()
                    self.offset_persister.persist(self.max_offsets)
                    self.commit_offsets()
                    buffer = []
                    gc.collect()
                continue

            if msg.error():
                logging.error(f'AvroConsumer error: {msg.error()}')
                continue

            next_offset = msg.offset() + 1
            self.max_offsets[msg.partition()] = Offset(
                offset=next_offset, persisted=False
            )

            buffer.append(msg.value())
            logging.info(f'Buffering message {msg.partition()}:{msg.offset()}...')
            if len(buffer) == MAX_BUFFER_SIZE:
                logging.warning(f'Ingesting full buffer ::: Buffer length: {MAX_BUFFER_SIZE}...')
                self.courier.collect_messages(buffer)
                self.courier.deliver()
                self.mark_revoked_as_persisted()
                self.offset_persister.persist(self.max_offsets)
                self.commit_offsets()
                buffer = []
                gc.collect()

        self.consumer.close()

    def consume_until(self, minutes=5, memory=250):
        """Executes the consumer infinite loop (delivery based on time and memory)"""

        buffer = []
        buffer_size = 0
        init_time = time.time()

        while True:

            try:
                msg = self.consumer.poll(2)
            except SerializerError as e:
                logging.error(f'Deserialization failed for {msg}: {e}')
                break

            if buffer and self._get_time_diff(init_time, time.time()) > minutes:
                logging.warning(f'Ingesting by time reached ::: Buffer length: {len(buffer)}')
                self.courier.collect_messages(buffer)
                self.courier.deliver()
                self.mark_revoked_as_persisted()
                self.offset_persister.persist(self.max_offsets)
                self.commit_offsets()
                del buffer
                gc.collect()
                buffer = []
                buffer_size = 0
                init_time = time.time()

            if msg is None:
                logging.info(f'No message in this poll ::: Buffer length: {len(buffer)}')
                continue

            if msg.error():
                logging.error(f'AvroConsumer error: {msg.error()}')
                continue

            next_offset = msg.offset() + 1
            self.max_offsets[msg.partition()] = Offset(
                offset=next_offset, persisted=False
            )

            buffer.append(msg.value())
            buffer_size += self._get_message_size_in_mb(msg.value())

            logging.info(f'Buffering message {msg.partition()}:{msg.offset()}...')

            if buffer_size > memory:
                logging.warning(f'Ingesting by memory reached ::: Buffer length: {len(buffer)}')
                self.courier.collect_messages(buffer)
                self.courier.deliver()
                self.mark_revoked_as_persisted()
                self.offset_persister.persist(self.max_offsets)
                self.commit_offsets()
                del buffer
                gc.collect()
                buffer = []
                buffer_size = 0
                init_time = time.time()

        self.consumer.close()
