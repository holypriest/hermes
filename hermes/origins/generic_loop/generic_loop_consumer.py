from time import sleep

class GenericLoopConsumer:

    def __init__(
            self,
            courier,
            consumer_function,
            consumer_function_args={},
            wait_time_seconds=0,
        ):
            self.courier = courier
            self.consumer_function = consumer_function
            self.consumer_function_args = consumer_function_args
            self.wait_time_seconds = wait_time_seconds
            self.RUNNING = True

    @property
    def is_running(self):
        return self.RUNNING

    def consume(self):
        """Executes the consumer infinite loop (delivery based on buffer length)"""

        buffer = []

        while self.is_running:

            buffer = self.consumer_function(**self.consumer_function_args)
            self.courier.collect_messages(buffer)
            self.courier.deliver()
            buffer = []
            sleep(self.wait_time_seconds)
