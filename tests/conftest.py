import boto3
from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
import psycopg2
import pytest
from time import sleep
from hermes.config import KAFKA_DYNAMODB_OFFSETS_TABLE, \
    KINESIS_STREAM, KAFKA_TOPIC, S3_BUCKET
from hermes.destinations.courier import Courier
from hermes.origins.kafka.offset.offset_persister import OffsetPersister


@pytest.fixture(scope='module')
def create_table(request):

    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )

    dev_dynamodb.create_table(
        TableName=KAFKA_DYNAMODB_OFFSETS_TABLE,
        AttributeDefinitions=[
            {
                'AttributeName': 'topic',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'partition',
                'AttributeType': 'N'
            }
        ],
        KeySchema=[
            {
                'AttributeName': 'topic',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'partition',
                'KeyType': 'RANGE'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

    sleep(2)  # Wait for stream creation

    def delete_table():
        table = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
        table.delete()
        sleep(2)

    request.addfinalizer(delete_table)


@pytest.fixture(scope='module')
def create_stream(request):

    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )

    dev_kinesis.create_stream(
        StreamName=KINESIS_STREAM,
        ShardCount=4
    )
    sleep(2)  # Wait for stream creation

    def delete_stream():
        dev_kinesis.delete_stream(
            StreamName=KINESIS_STREAM,
            EnforceConsumerDeletion=True
        )
        sleep(2)

    request.addfinalizer(delete_stream)

@pytest.fixture(scope='module')
def create_bucket(request):

    dev_s3 = boto3.resource(
        's3',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )

    dev_s3.Bucket(S3_BUCKET).create()
    sleep(2)  # Wait for bucket creation

    def delete_bucket():
        bucket = dev_s3.Bucket(S3_BUCKET)
        bucket.delete_objects(
            Delete={
                'Objects': [{'Key': obj.key}] for obj in bucket.objects.all()
            }
        )
        dev_s3.Bucket(S3_BUCKET).delete()

    request.addfinalizer(delete_bucket)

@pytest.fixture(scope='module')
def get_courier():
    dev_kinesis = boto3.client(
        'kinesis',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )
    c = Courier('kinesis')
    c.courier.client = dev_kinesis
    return c


@pytest.fixture(scope='module')
def get_persister():
    dev_dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-east-1',
        endpoint_url='http://localhost:4566',
        aws_access_key_id='test',
        aws_secret_access_key='test'
    )
    t = dev_dynamodb.Table(KAFKA_DYNAMODB_OFFSETS_TABLE)
    offp = OffsetPersister('dynamodb', KAFKA_TOPIC)
    offp.persister.table = t
    return offp


@pytest.fixture(scope='function')
def create_topic():
    a = AdminClient({'bootstrap.servers': 'localhost:9092'})
    new_topic = [
        NewTopic("test-topic", num_partitions=20, replication_factor=1)
    ]
    fs = a.create_topics(new_topic)
    for topic, f in fs.items():
        try:
            f.result()
        except Exception:
            pass
    sleep(2)


@pytest.fixture(scope='function')
def publish():
    value_schema_str = """
    {
        "type": "record",
        "name": "topicValue",
        "namespace": "com.landoop.topic",
        "fields": [
            {
            "name": "id",
            "type": "string"
            }
        ]
    }
    """
    value_schema = avro.loads(value_schema_str)
    avroProducer = AvroProducer(
        {
            'bootstrap.servers': 'localhost:9092',
            'on_delivery': lambda e, msg: None,
            'schema.registry.url': 'http://localhost:8081'
        },
        default_value_schema=value_schema
    )
    for i in range(10000):
        value = {"id": f"msg-{i}"}
        avroProducer.produce(topic='test-topic', value=value)
        if i % 100 == 0:
            avroProducer.flush()


@pytest.fixture(scope='function')
def prepare_topic(create_topic, publish, request):
    def delete_topic():
        a = AdminClient({'bootstrap.servers': 'localhost:9092'})
        sr = SchemaRegistryClient({'url': 'http://localhost:8081'})
        topic = ["test-topic"]
        fs = a.delete_topics(topic)
        for topic, f in fs.items():
            try:
                f.result()
            except Exception:
                pass
        sr.delete_subject('test-topic-value')
    request.addfinalizer(delete_topic)

@pytest.fixture(scope='function')
def create_postgres_table(request):
    connection = psycopg2.connect(
            host='localhost',
            database='postgres',
            user='postgres',
            password='secret',
        )

    create_table_stmt = '''
        CREATE TABLE test (
            int_col INTEGER PRIMARY KEY,
            double_col DOUBLE PRECISION,
            string_col TEXT,
            date_col DATE,
            timestamp_col TIMESTAMP,
            json_col JSON
        );
    '''
    with connection.cursor() as cursor:
        cursor.execute(create_table_stmt);
        connection.commit()

    sleep(2)  # Wait for stream creation

    def delete_table():
        with connection.cursor() as cursor:
            cursor.execute('DROP TABLE test');
            connection.commit()
        sleep(2)

    request.addfinalizer(delete_table)
