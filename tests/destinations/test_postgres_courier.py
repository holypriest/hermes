from datetime import datetime, date
from hermes.destinations.postgres_courier import PostgresCourier
import json
import psycopg2
import psycopg2.extras


def test_deliver(create_postgres_table):
    buffer = [
        {
            'int_col': 1,
            'double_col': 1.1,
            'string_col': 'foo',
            'date_col': date(1971, 1, 1),
            'timestamp_col': datetime(1971, 1, 1, 1, 1, 1, 111111),
            'json_col': '{"json_col1": 1, "json_col2": "bar"}'
        },
        {
            'int_col': 2,
            'double_col': 2.234,
            'string_col': 'bar',
            'date_col': date(1963, 1, 1),
            'timestamp_col': datetime(1963, 1, 1, 1, 1, 1, 111111),
            'json_col': '{"json_col1": 1, "json_col2": "bar"}'
        }
    ]
    pc = PostgresCourier(
        table='test',
        fields=[
            'int_col',
            'double_col',
            'string_col',
            'date_col',
            'timestamp_col',
            'json_col'
        ]
    )
    pc.collect_messages(buffer)
    r = pc.deliver()

    connection = psycopg2.connect(
        host='localhost',
        database='postgres',
        user='postgres',
        password='secret',
    )

    with connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor) as cursor:
        select_stmt = 'SELECT * FROM test'
        cursor.execute(select_stmt)
        connection.commit()
        results = cursor.fetchall()
        messages_retrieved = []
        for row in results:
            messages_retrieved.append(row)

    assert r['status'] == 'delivered'
    assert not pc.messages

    for i in range(len(buffer)):
        assert buffer[i]['int_col'] == messages_retrieved[i]['int_col']
        assert buffer[i]['double_col'] == messages_retrieved[i]['double_col']
        assert buffer[i]['string_col'] == messages_retrieved[i]['string_col']
        assert buffer[i]['date_col'] == messages_retrieved[i]['date_col']
        assert buffer[i]['timestamp_col'] == messages_retrieved[i]['timestamp_col']
        assert buffer[i]['json_col'] == json.dumps(messages_retrieved[i]['json_col'])

def test_deliver_duplicates(create_postgres_table):
    buffer = [
        {
            'int_col': 1,
            'double_col': 1.1,
            'string_col': 'foo',
            'date_col': date(1971, 1, 1),
            'timestamp_col': datetime(1971, 1, 1, 1, 1, 1, 111111),
            'json_col': '{"json_col1": 1, "json_col2": "bar"}'
        },
        {
            'int_col': 1,
            'double_col': 1.1,
            'string_col': 'foo',
            'date_col': date(1971, 1, 1),
            'timestamp_col': datetime(1971, 1, 1, 1, 1, 1, 111111),
            'json_col': '{"json_col1": 1, "json_col2": "bar"}'
        }
    ]
    pc = PostgresCourier(
        table='test',
        fields=[
            'int_col',
            'double_col',
            'string_col',
            'date_col',
            'timestamp_col',
            'json_col'
        ]
    )
    pc.collect_messages(buffer)
    r = pc.deliver()

    connection = psycopg2.connect(
            host='localhost',
            database='postgres',
            user='postgres',
            password='secret',
        )

    with connection.cursor(cursor_factory = psycopg2.extras.RealDictCursor) as cursor:
        select_stmt = 'SELECT * FROM test'
        cursor.execute(select_stmt)
        connection.commit()
        results = cursor.fetchall()
        messages_retrieved = []
        for row in results:
            messages_retrieved.append(row)

    assert r['status'] == 'delivered'
    assert not pc.messages
    assert len(messages_retrieved) == 1

    deduplicated_message = messages_retrieved[0]
    assert buffer[1]['int_col'] == deduplicated_message['int_col']
    assert buffer[1]['double_col'] == deduplicated_message['double_col']
    assert buffer[1]['string_col'] == deduplicated_message['string_col']
    assert buffer[1]['date_col'] == deduplicated_message['date_col']
    assert buffer[1]['timestamp_col'] == deduplicated_message['timestamp_col']
    assert buffer[1]['json_col'] == json.dumps(deduplicated_message['json_col'])
