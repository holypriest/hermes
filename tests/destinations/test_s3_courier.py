import boto3
from hermes.destinations.s3_courier import S3Courier
from hermes.config import S3_BUCKET
from unittest.mock import patch

dev_s3 = boto3.resource(
    's3',
    region_name='us-east-1',
    endpoint_url='http://localhost:4566',
    aws_access_key_id='test',
    aws_secret_access_key='test'
)


def get_dev_resource(self, region):
    return dev_s3

def test_collect_messages():
    buffer = [{'foo': 'bar'}, {'foo': 'baz'}]
    s3c = S3Courier()
    s3c.collect_messages(buffer)
    assert s3c.messages is buffer

def test_collect_tampered_messages():
    buffer = [{'foo': 'bar'}, {'foo': 'baz'}]
    swap = lambda msg: {v: k for k, v in msg.items()}
    s3c = S3Courier(tamper_function=swap)
    s3c.collect_messages(buffer)
    assert s3c.messages == [swap(msg) for msg in buffer]

@patch.object(
    S3Courier,
    '_get_resource',
    get_dev_resource
)
def test_deliver(create_bucket):
    buffer = [{'foo': 'bar'}, {'foo': 'baz'}]
    s3c = S3Courier()
    s3c.collect_messages(buffer)
    r = s3c.deliver()

    assert r['status'] == 'delivered'
    assert not s3c.messages

    bucket = dev_s3.Bucket(S3_BUCKET)
    print(list(bucket.objects.all()))
    assert len(list(bucket.objects.all())) > 0
