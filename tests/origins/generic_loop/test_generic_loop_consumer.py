from hermes.origins.generic_loop.generic_loop_consumer import GenericLoopConsumer
from unittest.mock import PropertyMock, MagicMock, patch
from uuid import uuid4

def test_consume():

    cour = MagicMock()

    def foo_func():
        return []

    glc = GenericLoopConsumer(
        courier=cour,
        consumer_function=foo_func,
        wait_time_seconds=1
    )

    with patch(
        'hermes.origins.generic_loop.generic_loop_consumer.GenericLoopConsumer.is_running',
        new_callable=PropertyMock,
        side_effect=[True, False]
    ) as p:

        glc.consume()

    glc.courier.deliver.assert_called_once()
