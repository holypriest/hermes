from hermes.config import KAFKA_TOPIC
from hermes.origins.kafka.avro_consumer import AvroConsumer
from confluent_kafka import TopicPartition
import multiprocessing
from time import sleep


consumer_conf = {
    'bootstrap.servers': 'localhost:9092',
    'group.id': 'hermes-group',
    'enable.auto.commit': 'false'
}

sr_conf = {
    'url': 'http://localhost:8081'
}


def test_consume(
    create_stream,
    create_table,
    prepare_topic,
    get_courier,
    get_persister
):

    cour = get_courier
    offp = get_persister

    def _on_revoke(consumer, partitions):
        pass

    def _on_assign(consumer, partitions):
        topic = partitions[0].topic
        partition_numbers = [p.partition for p in partitions]
        op = offp
        loff = op.get_last_offsets(partition_numbers)
        consumer.assign(
            [TopicPartition(topic, part, offs) for part, offs in loff.items()]
        )
        print(f'New partition assignment: {loff}')

    def create_and_consume():
        ac = AvroConsumer(
            consumer_conf=consumer_conf,
            sr_conf=sr_conf,
            topic=KAFKA_TOPIC,
            courier=cour,
            offset_persister=offp,
            value_schema_subject=KAFKA_TOPIC+'-value'
        )
        ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
        ac.consume()

    p = multiprocessing.Process(target=create_and_consume, args=())
    p.start()
    sleep(5)
    p.terminate()

    last_offsets = offp.get_last_offsets(
        [i for i in range(0, 20)]
    )

    assert sum([v for _, v in last_offsets.items()]) > 20
    assert sum([v for _, v in last_offsets.items()]) <= 10000


def test_consume_until_by_time(
    create_stream,
    create_table,
    prepare_topic,
    get_courier,
    get_persister
):

    cour = get_courier
    offp = get_persister

    def _on_revoke(consumer, partitions):
        pass

    def _on_assign(consumer, partitions):
        topic = partitions[0].topic
        partition_numbers = [p.partition for p in partitions]
        op = offp
        loff = op.get_last_offsets(partition_numbers)
        consumer.assign(
            [TopicPartition(topic, part, offs) for part, offs in loff.items()]
        )
        print(f'New partition assignment: {loff}')

    def create_and_consume():
        ac = AvroConsumer(
            consumer_conf=consumer_conf,
            sr_conf=sr_conf,
            topic=KAFKA_TOPIC,
            courier=cour,
            offset_persister=offp,
            value_schema_subject=KAFKA_TOPIC+'-value'
        )
        ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
        ac.consume_until(minutes=0.033, memory=500)

    p = multiprocessing.Process(target=create_and_consume, args=())
    p.start()
    sleep(1)
    p.terminate()

    last_offsets = offp.get_last_offsets(
        [i for i in range(0, 20)]
    )

    assert sum([v for _, v in last_offsets.items()]) > 20
    assert sum([v for _, v in last_offsets.items()]) <= 10000

def test_consume_until_by_memory(
    create_stream,
    create_table,
    prepare_topic,
    get_courier,
    get_persister
):

    cour = get_courier
    offp = get_persister

    def _on_revoke(consumer, partitions):
        pass

    def _on_assign(consumer, partitions):
        topic = partitions[0].topic
        partition_numbers = [p.partition for p in partitions]
        op = offp
        loff = op.get_last_offsets(partition_numbers)
        consumer.assign(
            [TopicPartition(topic, part, offs) for part, offs in loff.items()]
        )
        print(f'New partition assignment: {loff}')

    def create_and_consume():
        ac = AvroConsumer(
            consumer_conf=consumer_conf,
            sr_conf=sr_conf,
            topic=KAFKA_TOPIC,
            courier=cour,
            offset_persister=offp,
            value_schema_subject=KAFKA_TOPIC+'-value'
        )
        ac.subscribe(on_assign=_on_assign, on_revoke=_on_revoke)
        ac.consume_until(minutes=5, memory=0.05)

    p = multiprocessing.Process(target=create_and_consume, args=())
    p.start()
    sleep(5)
    p.terminate()

    last_offsets = offp.get_last_offsets(
        [i for i in range(0, 20)]
    )

    assert sum([v for _, v in last_offsets.items()]) > 20
    assert sum([v for _, v in last_offsets.items()]) <= 10000
